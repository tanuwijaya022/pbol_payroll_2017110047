-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2020 at 01:00 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payroll_2017110047`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `nomor` varchar(50) DEFAULT NULL,
  `periode` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `tanggal`, `nomor`, `periode`) VALUES
(5, '2020-03-26', 'A-2020/03/2', '2020-03-01'),
(6, '2020-04-06', 'A-2020/04/1', '2020-03-31'),
(7, '2020-04-12', 'A-2020/04/2', '2020-04-30'),
(8, '2020-04-12', 'A-2020/04/3', '2020-05-31');

-- --------------------------------------------------------

--
-- Table structure for table `detil_absensi`
--

CREATE TABLE `detil_absensi` (
  `id` int(11) NOT NULL,
  `id_absensi` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `JHK` int(11) DEFAULT NULL,
  `JMK` int(11) DEFAULT NULL,
  `jml_lembur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detil_absensi`
--

INSERT INTO `detil_absensi` (`id`, `id_absensi`, `id_pegawai`, `JHK`, `JMK`, `jml_lembur`) VALUES
(6, 5, 1, 24, 24, 12),
(7, 6, 2, 24, 24, 2),
(8, 6, 3, 24, 24, 1),
(9, 7, 1, 24, 1, 0),
(10, 7, 2, 24, 23, 0),
(11, 7, 3, 24, 3, 0),
(15, 8, 1, 30, 30, 0),
(16, 8, 2, 30, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_jabatan`
--

CREATE TABLE `master_jabatan` (
  `idJabatan` int(11) NOT NULL,
  `kodeJabatan` varchar(10) DEFAULT NULL,
  `namaJabatan` varchar(50) DEFAULT NULL,
  `TunjJabatan` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_jabatan`
--

INSERT INTO `master_jabatan` (`idJabatan`, `kodeJabatan`, `namaJabatan`, `TunjJabatan`) VALUES
(1, 'AM', 'Area Manager', 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `master_pegawai`
--

CREATE TABLE `master_pegawai` (
  `idPegawai` int(11) NOT NULL,
  `kodePegawai` varchar(10) DEFAULT NULL,
  `namaPegawai` varchar(50) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `noHp` varchar(13) DEFAULT NULL,
  `idJabatan` int(11) DEFAULT NULL,
  `tglLahir` date DEFAULT NULL,
  `TunjKesehatan` double DEFAULT NULL,
  `Gapok` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_pegawai`
--

INSERT INTO `master_pegawai` (`idPegawai`, `kodePegawai`, `namaPegawai`, `alamat`, `noHp`, `idJabatan`, `tglLahir`, `TunjKesehatan`, `Gapok`) VALUES
(1, 'S0001', 'Didin', 'bandung', '0894684', 1, '1993-03-11', 250000, 3300000),
(2, 'S0002', 'Udin', 'jakarta', '067856', 1, '2020-04-04', 200000, 3000000),
(3, 'S0003', 'Asep', 'sumedang', '3453463', 1, '1998-10-20', 125000, 2800000);

-- --------------------------------------------------------

--
-- Table structure for table `upah_pegawai_detil`
--

CREATE TABLE `upah_pegawai_detil` (
  `id` int(11) NOT NULL,
  `id_upah` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `upah_lembur` double DEFAULT NULL,
  `tunj_jabatan` double DEFAULT NULL,
  `tunj_kesehatan` double DEFAULT NULL,
  `tunj_lain_lain` double DEFAULT NULL,
  `JHK` int(11) DEFAULT NULL,
  `Lembur` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `potongan_kehadiran` double DEFAULT NULL,
  `potongan_lain` double DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `gapok` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `upah_pegawai_detil`
--

INSERT INTO `upah_pegawai_detil` (`id`, `id_upah`, `id_pegawai`, `upah_lembur`, `tunj_jabatan`, `tunj_kesehatan`, `tunj_lain_lain`, `JHK`, `Lembur`, `total`, `potongan_kehadiran`, `potongan_lain`, `keterangan`, `gapok`) VALUES
(3, 2, 1, 200000, 0, 0, 0, 24, 12, 4800000, NULL, NULL, NULL, NULL),
(10, 4, 2, 32000, 2344.23, 2133, 324, 24, 2, 2468801, NULL, NULL, NULL, NULL),
(11, 4, 3, 50000, 342343, 43, 234, 24, 1, 3341421, NULL, NULL, NULL, NULL),
(18, 6, 1, 0, 1000000, 250000, 0, 1, 0, 1287500, 3162500, 100000, 'asdf', 3300000),
(19, 6, 2, 0, 1000000, 200000, 0, 23, 0, 3875000, 125000, 200000, '', 3000000),
(20, 6, 3, 0, 1000000, 125000, 0, 3, 0, 1475000, 2450000, 0, '', 2800000);

-- --------------------------------------------------------

--
-- Table structure for table `upah_pegawai_master`
--

CREATE TABLE `upah_pegawai_master` (
  `id_upah` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `nomor` varchar(50) DEFAULT NULL,
  `id_absensi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `upah_pegawai_master`
--

INSERT INTO `upah_pegawai_master` (`id_upah`, `tanggal`, `nomor`, `id_absensi`) VALUES
(2, '2020-03-26', 'U-2020/04/2', 5),
(4, '2020-04-10', 'U-2020/04/3', 6),
(6, '2020-04-12', 'U-2020/04/4', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `detil_absensi`
--
ALTER TABLE `detil_absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idabsensi` (`id_absensi`);

--
-- Indexes for table `master_jabatan`
--
ALTER TABLE `master_jabatan`
  ADD PRIMARY KEY (`idJabatan`);

--
-- Indexes for table `master_pegawai`
--
ALTER TABLE `master_pegawai`
  ADD PRIMARY KEY (`idPegawai`),
  ADD KEY `fk_idjabatan` (`idJabatan`);

--
-- Indexes for table `upah_pegawai_detil`
--
ALTER TABLE `upah_pegawai_detil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idupah` (`id_upah`);

--
-- Indexes for table `upah_pegawai_master`
--
ALTER TABLE `upah_pegawai_master`
  ADD PRIMARY KEY (`id_upah`),
  ADD KEY `fk_idabsensiupah` (`id_absensi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detil_absensi`
--
ALTER TABLE `detil_absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `master_jabatan`
--
ALTER TABLE `master_jabatan`
  MODIFY `idJabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_pegawai`
--
ALTER TABLE `master_pegawai`
  MODIFY `idPegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `upah_pegawai_detil`
--
ALTER TABLE `upah_pegawai_detil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `upah_pegawai_master`
--
ALTER TABLE `upah_pegawai_master`
  MODIFY `id_upah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detil_absensi`
--
ALTER TABLE `detil_absensi`
  ADD CONSTRAINT `fk_idabsensi` FOREIGN KEY (`id_absensi`) REFERENCES `absensi` (`id_absensi`);

--
-- Constraints for table `master_pegawai`
--
ALTER TABLE `master_pegawai`
  ADD CONSTRAINT `fk_idjabatan` FOREIGN KEY (`idJabatan`) REFERENCES `master_jabatan` (`idJabatan`);

--
-- Constraints for table `upah_pegawai_detil`
--
ALTER TABLE `upah_pegawai_detil`
  ADD CONSTRAINT `fk_idupah` FOREIGN KEY (`id_upah`) REFERENCES `upah_pegawai_master` (`id_upah`);

--
-- Constraints for table `upah_pegawai_master`
--
ALTER TABLE `upah_pegawai_master`
  ADD CONSTRAINT `fk_idabsensiupah` FOREIGN KEY (`id_absensi`) REFERENCES `absensi` (`id_absensi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
