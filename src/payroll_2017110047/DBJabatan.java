/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Multimedia
 */
public class DBJabatan {
    private int idJabatan;
    private String kodeJabatan,namaJabatan;
    private Double TunjJabatan;

    public Double getTunjJabatan() {
        return TunjJabatan;
    }

    public void setTunjJabatan(Double TunjJabatan) {
        this.TunjJabatan = TunjJabatan;
    }

    public void GetDataJabatan(int dt){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idJabatan, kodeJabatan, namaJabatan, TunjJabatan "+
                    " from master_jabatan where idJabatan = "+dt);
            
          int i=1;
          while(rs.next()){
              this.idJabatan = rs.getInt("idJabatan");
              this.kodeJabatan = rs.getString("kodeJabatan");
              this.namaJabatan = rs.getString("namaJabatan");
              this.TunjJabatan = rs.getDouble("TunjJabatan");
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
    public int getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(int idJabatan) {
        this.idJabatan = idJabatan;
    }

    public String getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(String kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getNamaJabatan() {
        return namaJabatan;
    }

    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan;
    }
    
    public void GetAllField(String fld, String dt){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idJabatan, kodeJabatan, namaJabatan, TunjJabatan "+
                    " from master_jabatan where "+fld+" = '"+dt+"'");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              
              this.idJabatan=rs.getInt("idJabatan");
              this.kodeJabatan=rs.getString("kodeJabatan");
              this.namaJabatan=rs.getString("namaJabatan");
              this.TunjJabatan=rs.getDouble("TunjJabatan");
              
//tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    
    public Vector LookUp(String dt){
        try{
            Vector tableData= new Vector();
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idJabatan, kodeJabatan, namaJabatan "+
                    " from master_jabatan where upper(namaJabatan) like upper('%"+dt+"%')");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("idJabatan"));
              row.add(rs.getString("kodeJabatan"));
              row.add(rs.getString("namaJabatan"));
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          if (i>1) return tableData;
          else return null;
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    } 
    
    /*public static String LookUp(String fld, String dt){
        try{
            String hasil="";
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idBarang, NamaBarang, harga "+
                    " from barang where "+fld+" = '%"+dt+"%'");
            
          int i=1;
          while(rs.next()){
              hasil=(rs.getString("idBarang"))+(rs.getString("NamaBarang"))+(rs.getFloat("harga"));
              i++;
          }
          con.tutupKoneksi();
          return hasil;
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }*/
    
    public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into master_jabatan"+
                    "(kodeJabatan, namaJabatan,TunjJabatan) values (?,?,?)");
                     con.preparedStatement.setString(1, this.kodeJabatan);
                     con.preparedStatement.setString(2, this.namaJabatan);
                     con.preparedStatement.setDouble(3, this.TunjJabatan);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    public Vector Load(){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select idJabatan, kodeJabatan, namaJabatan "+
                  " from master_jabatan");
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("idJabatan"));
              row.add(rs.getString("kodeJabatan"));
              row.add(rs.getString("namaJabatan"));
              tableData.add(row);
              i++;
          }
          if(i==1){ 
              Vector<Object> row = new Vector<Object>();
              tableData.add(row);
          }
          
          
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    public boolean delete(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("delete from master_jabatan where idJabatan"+
                    " = ? ");
            con.preparedStatement.setInt(1, id);
            con.preparedStatement.executeUpdate();
            berhasil = true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
    public boolean select(int id){
        try {
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idJabatan, kodeJabatan, namaJabatan, TunjJabatan "+
                    " from master_jabatan where idJabatan = "+id);
            while (rs.next()){
              this.idJabatan=rs.getInt("idJabatan");
              this.kodeJabatan=rs.getString("kodeJabatan");
              this.namaJabatan=rs.getString("namaJabatan");
              this.TunjJabatan=rs.getDouble("TunjJabatan");
            }
            con.tutupKoneksi();
            return true;
        } catch(SQLException ex){
            ex.printStackTrace();
            return false;
        }
    }
    
    public int validasiJabatan(String nama, int idKecuali){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("Select count(*) as jml "+
         " from master_jabatan where kodeJabatan = '"+nama+"' and idJabatan <> "+idKecuali);
            while(rs.next()){
                val = rs.getInt("jml");
            }
            con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();                    
        }

        return val;
    }
    
    public boolean update(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("update master_jabatan set kodejabatan=?, namaJabatan=?, TunjJabatan=? "+
                    " where idJabatan = ?");
                     con.preparedStatement.setString(1, this.kodeJabatan);
                     con.preparedStatement.setString(2, this.namaJabatan);
                     con.preparedStatement.setDouble(3, this.TunjJabatan);
                     con.preparedStatement.setInt(4, this.idJabatan);
                     con.preparedStatement.executeUpdate();
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
}
