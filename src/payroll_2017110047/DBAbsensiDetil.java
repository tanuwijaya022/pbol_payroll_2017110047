/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Multimedia
 */
public class DBAbsensiDetil {
    private int id_absensi;
    private int id_pegawai;
    private int JHK;
    private int JMK;
    private int jml_lembur;

    public int getId_absensi() {
        return id_absensi;
    }

    public void setId_absensi(int id_absensi) {
        this.id_absensi = id_absensi;
    }

    public int getId_pegawai() {
        return id_pegawai;
    }

    public void setId_pegawai(int id_pegawai) {
        this.id_pegawai = id_pegawai;
    }

    public int getJHK() {
        return JHK;
    }

    public void setJHK(int JHK) {
        this.JHK = JHK;
    }

    public int getJMK() {
        return JMK;
    }

    public void setJMK(int JMK) {
        this.JMK = JMK;
    }

    public int getJml_lembur() {
        return jml_lembur;
    }

    public void setJml_lembur(int jml_lembur) {
        this.jml_lembur = jml_lembur;
    }
    
    public Vector LoadDetail(int id, boolean forUpah){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select d.id_pegawai as idpeg, p.kodePegawai as nip,p.namaPegawai as nama, JHK, JMK, jml_lembur,Gapok, p.tunjKesehatan, j.tunjJabatan "+
                  ", ((gapok/JHK)*(JHK-JMK)) as potKehadiran, (Gapok+p.tunjKesehatan+j.tunjJabatan-((gapok/JHK)*(JHK-JMK))) as total "+
                  " from detil_absensi d, master_pegawai p, master_jabatan j where j.idJabatan = p.idJabatan and p.idpegawai = d.id_pegawai and id_absensi = "+id);
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              if(forUpah){
                row.add(i);
                row.add(rs.getInt("idpeg"));
                row.add(rs.getString("nip"));
                row.add(rs.getString("nama"));
                row.add(rs.getInt("JMK"));
                row.add(rs.getInt("jml_lembur"));    
                row.add(rs.getDouble("Gapok"));
                row.add(0);
                row.add(rs.getDouble("tunjJabatan"));
                row.add(rs.getDouble("tunjKesehatan"));
                row.add(0);
                row.add(rs.getDouble("potKehadiran"));   
                row.add(0);
                row.add(rs.getDouble("total"));              
              } else {
                row.add(i);
                row.add(rs.getInt("idpeg"));
                row.add(rs.getString("nip"));
                row.add(rs.getString("nama"));
                row.add(rs.getInt("JHK"));
                row.add(rs.getInt("JMK"));
                row.add(rs.getInt("jml_lembur"));
              }
              tableData.add(row);
              
              i++;
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
}


