/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author Multimedia
 */
public class DBAbsensi {
    private int id_absensi;
    private Date tanggal;
    private String nomor;
    private Date periode;
    
    private ArrayList<DBAbsensiDetil> absensi_detil;    
    
   
    public DBAbsensi(){
        absensi_detil = new ArrayList<DBAbsensiDetil>();
    } 

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }
    

    public int getId_absensi() {
        return id_absensi;
    }

    public void setId_absensi(int id_absensi) {
        this.id_absensi = id_absensi;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getPeriode() {
        return periode;
    }

    public void setPeriode(Date periode) {
        this.periode = periode;
    }

    public ArrayList<DBAbsensiDetil> getAbsensi_detil() {
        return absensi_detil;
    }

    public void setAbsensi_detil(ArrayList<DBAbsensiDetil> absensi_detil) {
        this.absensi_detil = absensi_detil;
    }

    public void AddAbsensiDetil(DBAbsensiDetil AD) {
        this.absensi_detil.add(AD);
    }
    
    public void GetDataAbsensi(int dt){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id_absensi,tanggal, periode, nomor"+
                    " from absensi where id_absensi = "+dt);
            
          int i=1;
          while(rs.next()){
              this.id_absensi = rs.getInt("id_absensi");
              this.tanggal = rs.getDate("tanggal");
              this.periode = rs.getDate("periode");
              this.nomor = rs.getString("nomor");
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
    public Vector LookUp(){
        try{
            Vector tableData= new Vector();
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select id_absensi, tanggal, nomor, periode "+
                  " from absensi a where a.id_absensi not in (select u.id_absensi from upah_pegawai_master u)");
          int i=1;
          SimpleDateFormat format = new SimpleDateFormat("MMMM YYYY");
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("id_absensi"));
              row.add(rs.getDate("tanggal"));
              row.add(rs.getString("nomor"));
              row.add(format.format(rs.getDate("periode")));

              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          if (i>1) return tableData;
          else return null;
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    } 
    
    public boolean insertMaster(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.dbKoneksi.setAutoCommit(false);
            con.statement = con.dbKoneksi.createStatement();
            String sql = "Insert into absensi"+
                         "(tanggal, periode, nomor) values ('"+
                         this.tanggal+"',LAST_DAY('"+this.periode+"'),'"+generate_nomor_otomatis()+"')"; 
            //con.preparedStatement.setDate(1, this.tanggal);
            //con.preparedStatement.setDate(2, this.periode);
            con.statement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet rs= con.statement.getGeneratedKeys();
            int last_insert_id = -1;
            if (rs.next()){
                last_insert_id=rs.getInt(1);
            }
            this.id_absensi = last_insert_id;
            
            for (int i=0; i<=absensi_detil.size()-1; i++){
                con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into detil_absensi"+
                  "(id_absensi, id_pegawai, JHK, JMK, jml_lembur) values (?,?,?,?,?)");
                   
                   DBAbsensiDetil dj = (DBAbsensiDetil)absensi_detil.get(i);
                   con.preparedStatement.setInt(1, this.id_absensi);
                   con.preparedStatement.setInt(2, dj.getId_pegawai());
                   con.preparedStatement.setInt(3, dj.getJHK());
                   con.preparedStatement.setInt(4, dj.getJMK());
                   con.preparedStatement.setInt(5, dj.getJml_lembur());
                   con.preparedStatement.executeUpdate();    
            }
                     
         con.dbKoneksi.commit();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            con.dbKoneksi.rollback();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    
    public boolean editMaster(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.dbKoneksi.setAutoCommit(false);
            con.statement = con.dbKoneksi.createStatement();
            String sql = "update absensi set "+
                         " tanggal='"+this.tanggal+"', periode=LAST_DAY('"+this.periode+
                    "') where id_absensi = "+this.id_absensi;
            con.statement.executeUpdate(sql);
            
            sql  = 
                    "delete from detil_absensi where id_absensi "+
                    " = "+this.id_absensi;
            con.statement.executeUpdate(sql);
                        
            for (int i=0; i<=absensi_detil.size()-1; i++){
                con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into detil_absensi"+
                  "(id_absensi, id_pegawai, JHK, JMK, jml_lembur) values (?,?,?,?,?)");
                   
                   DBAbsensiDetil dj = (DBAbsensiDetil)absensi_detil.get(i);
                   con.preparedStatement.setInt(1, this.id_absensi);
                   con.preparedStatement.setInt(2, dj.getId_pegawai());
                   con.preparedStatement.setInt(3, dj.getJHK());
                   con.preparedStatement.setInt(4, dj.getJMK());
                   con.preparedStatement.setInt(5, dj.getJml_lembur());
                   con.preparedStatement.executeUpdate();    
            }
                     
         con.dbKoneksi.commit();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            con.dbKoneksi.rollback();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    
    public String generate_nomor_otomatis(){
        try {
        String nomor="";
        Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("select distinct concat(subString(nomor,1,10), " +
"(select max(cast(substring(nomor,11,length(nomor)) as integer)) from absensi where subString(nomor,3,8) = (select(date_format(now(), '%Y/%m/'))))+1) " +
"from absensi where subString(nomor,3,8) = (select(date_format(now(), '%Y/%m/')))");
          
          if (rs.next()){
                nomor=rs.getString(1);
            }
          if(nomor.equalsIgnoreCase("")){
           rs = con.statement.executeQuery("select distinct concat('A-',(select(date_format(now(), '%Y/%m/'))),1) from master_pegawai");
           if (rs.next()){
                nomor=rs.getString(1);
            } 
          }
          con.tutupKoneksi();
          return nomor;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
        
    };
    public Vector Load(){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select id_absensi, tanggal, nomor, periode "+
                  " from absensi");
          int i=1;
          SimpleDateFormat format = new SimpleDateFormat("MMMM YYYY");
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("id_absensi"));
              row.add(rs.getDate("tanggal"));
              row.add(rs.getString("nomor"));
              row.add(format.format(rs.getDate("periode")));

              tableData.add(row);
              i++;
          }
          if(i==1){ 
              Vector<Object> row = new Vector<Object>();
              tableData.add(row);
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    
    public boolean is_sudah_di_pakai_upah(int id){
        int hasil=0;
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select count(*) as hasil "+
                  " from upah_pegawai_master where id_absensi = '"+id+"'");
            
          while(rs.next()){              
              hasil=rs.getInt("hasil");
          }
          con.tutupKoneksi();
          
        } catch (SQLException ex){
            ex.printStackTrace();
            hasil = 1;
        }
        return hasil > 0;
    }
            
    
    public boolean delete(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.dbKoneksi.setAutoCommit(false);
            con.statement = con.dbKoneksi.createStatement();
            String sql  = 
                    "delete from detil_absensi where id_absensi "+
                    " = "+id;
            con.statement.executeUpdate(sql);
            
            sql  = 
                    "delete from absensi where id_absensi "+
                    " = "+id;
            con.statement.executeUpdate(sql);
            
            con.dbKoneksi.commit();
            berhasil = true;
        } catch(Exception e){
            e.printStackTrace();
            con.dbKoneksi.rollback();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    public void Get_absensi_master(int id){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id_absensi, tanggal, nomor, periode "+
                  " from absensi where id_absensi = '"+id+"'");
            
          int i=1;
          while(rs.next()){
              
              this.id_absensi=rs.getInt("id_absensi");
              this.tanggal=rs.getDate("tanggal");
              this.nomor=rs.getString("nomor");
              this.periode=rs.getDate("periode");
              
//tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
}
