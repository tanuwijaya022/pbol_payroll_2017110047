/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author hendry
 */
public class DBUpahpegawaidetil {
    
    private int id_upah;
    private int id_pegawai, JHK,Lembur;
    private double gapok, upah_lembur,
            tunj_jabatan, tunj_kesehatan,
            tunj_lain_lain, potongan_kehadiran, potongan_lain, total;
    private String keterangan;

    public int getId_upah() {
        return id_upah;
    }

    public void setId_upah(int id_upah) {
        this.id_upah = id_upah;
    }

    public int getId_pegawai() {
        return id_pegawai;
    }

    public int getJHK() {
        return JHK;
    }

    public void setJHK(int JHK) {
        this.JHK = JHK;
    }

    public int getLembur() {
        return Lembur;
    }

    public void setLembur(int Lembur) {
        this.Lembur = Lembur;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }


    public void setId_pegawai(int id_pegawai) {
        this.id_pegawai = id_pegawai;
    }
    
    

    public double getgapok() {
        return gapok;
    }

    public void setgapok(double gapok) {
        this.gapok = gapok;
    }

    

    public double getpotongan_kehadiran() {
        return potongan_kehadiran;
    }

    public void setpotongan_kehadiran(double potongan_kehadiran) {
        this.potongan_kehadiran = potongan_kehadiran;
    }

    
    public double getpotongan_lain() {
        return potongan_lain;
    }

    public void setpotongan_lain(double potongan_lain) {
        this.potongan_lain = potongan_lain;
    }
    
    public String getketerangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    
    
    public double getUpah_lembur() {
        return upah_lembur;
    }

    public void setUpah_lembur(double upah_lembur) {
        this.upah_lembur = upah_lembur;
    }

    public double getTunj_jabatan() {
        return tunj_jabatan;
    }

    public void setTunj_jabatan(double tunj_jabatan) {
        this.tunj_jabatan = tunj_jabatan;
    }

    public double getTunj_kesehatan() {
        return tunj_kesehatan;
    }

    public void setTunj_kesehatan(double tunj_kesehatan) {
        this.tunj_kesehatan = tunj_kesehatan;
    }

    public double getTunj_lain_lain() {
        return tunj_lain_lain;
    }

    public void setTunj_lain_lain(double tunj_lain_lain) {
        this.tunj_lain_lain = tunj_lain_lain;
    }
    
    
    public Vector LoadDetail(int id){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select d.id_pegawai as idpeg, p.kodePegawai as nip,p.namaPegawai as nama, JHK,Lembur, total, d.gapok, upah_lembur," +
"            d.tunj_jabatan, d.tunj_kesehatan," +
"            tunj_lain_lain, potongan_kehadiran, potongan_lain, keterangan from upah_pegawai_detil d, master_pegawai p where p.idpegawai = d.id_pegawai and id_upah = "+id);
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("idpeg"));
              row.add(rs.getString("nip"));
              row.add(rs.getString("nama"));
              row.add(rs.getInt("JHK"));
              row.add(rs.getInt("Lembur"));
              row.add(rs.getDouble("gapok"));
              row.add(rs.getDouble("upah_lembur"));
              row.add(rs.getDouble("tunj_jabatan"));
              row.add(rs.getDouble("tunj_kesehatan"));
              row.add(rs.getDouble("tunj_lain_lain"));
              row.add(rs.getDouble("potongan_kehadiran"));
              row.add(rs.getDouble("potongan_lain"));
              row.add(rs.getDouble("total"));
              row.add(rs.getString("keterangan"));
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    
}
