/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author hendry
 */
public class DBUpahpegawai {
    
    private int id_upah;
    private Date tanggal;
    private String nomor;
    private int id_absensi;
    
    private ArrayList<DBUpahpegawaidetil> upah_detil;    

    public int getId_absensi() {
        return id_absensi;
    }

    public void setId_absensi(int id_absensi) {
        this.id_absensi = id_absensi;
    }
    
   
    public DBUpahpegawai(){
        upah_detil = new ArrayList<DBUpahpegawaidetil>();
    } 
    
    public void AddUpahDetail(DBUpahpegawaidetil AD) {
        this.upah_detil.add(AD);
    }

    public int getId_upah() {
        return id_upah;
    }

    public void setId_upah(int id_upah) {
        this.id_upah = id_upah;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public ArrayList<DBUpahpegawaidetil> getUpah_detil() {
        return upah_detil;
    }

    public void setUpah_detil(ArrayList<DBUpahpegawaidetil> upah_detil) {
        this.upah_detil = upah_detil;
    }
    
    public boolean insertMaster(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.dbKoneksi.setAutoCommit(false);
            con.statement = con.dbKoneksi.createStatement();
            String sql = "Insert into upah_pegawai_master"+
                         "(tanggal, nomor, id_absensi) values ('"+
                         this.tanggal+"','"+generate_nomor_otomatis()+"',"+this.id_absensi+")"; 
            //con.preparedStatement.setDate(1, this.tanggal);
            //con.preparedStatement.setDate(2, this.periode);
            con.statement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet rs= con.statement.getGeneratedKeys();
            int last_insert_id = -1;
            if (rs.next()){
                last_insert_id=rs.getInt(1);
            }
            this.id_upah = last_insert_id;
            
            for (int i=0; i<=upah_detil.size()-1; i++){
                con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into upah_pegawai_detil "+
                  "(id_upah, id_pegawai, JHK,Lembur, total, gapok, upah_lembur," +
        "            tunj_jabatan, tunj_kesehatan," +
        "            tunj_lain_lain, potongan_kehadiran, potongan_lain, keterangan) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                   
                   DBUpahpegawaidetil dj = (DBUpahpegawaidetil)upah_detil.get(i);
                   con.preparedStatement.setInt(1, this.id_upah);
                   con.preparedStatement.setInt(2, dj.getId_pegawai());
                   con.preparedStatement.setInt(3, dj.getJHK());
                   con.preparedStatement.setInt(4, dj.getLembur());
                   con.preparedStatement.setDouble(5, dj.getTotal());
                   con.preparedStatement.setDouble(6, dj.getgapok());
                   con.preparedStatement.setDouble(7, dj.getUpah_lembur());
                   con.preparedStatement.setDouble(8, dj.getTunj_jabatan());
                   con.preparedStatement.setDouble(9, dj.getTunj_kesehatan());
                   con.preparedStatement.setDouble(10, dj.getTunj_lain_lain());
                   con.preparedStatement.setDouble(11, dj.getpotongan_kehadiran());
                   con.preparedStatement.setDouble(12, dj.getpotongan_lain());
                   con.preparedStatement.setString(13, dj.getketerangan());
                   con.preparedStatement.executeUpdate();    
            }
                     
         con.dbKoneksi.commit();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            con.dbKoneksi.rollback();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    
    public boolean editMaster(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.dbKoneksi.setAutoCommit(false);
            con.statement = con.dbKoneksi.createStatement();
            String sql = "update upah_pegawai_master set "+
                         " tanggal='"+this.tanggal+"' where id_upah = "+this.id_upah;
            con.statement.executeUpdate(sql);
            
            sql  = 
                    "delete from upah_pegawai_detil where id_upah "+
                    " = "+this.id_upah;
            con.statement.executeUpdate(sql);
                        
            for (int i=0; i<=upah_detil.size()-1; i++){
                con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into upah_pegawai_detil "+
                  "(id_upah, id_pegawai, JHK,Lembur, total, gapok, upah_lembur," +
        "            tunj_jabatan, tunj_kesehatan," +
        "            tunj_lain_lain, potongan_kehadiran, potongan_lain, keterangan) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                   
                   DBUpahpegawaidetil dj = (DBUpahpegawaidetil)upah_detil.get(i);
                   con.preparedStatement.setInt(1, this.id_upah);
                   con.preparedStatement.setInt(2, dj.getId_pegawai());
                   con.preparedStatement.setInt(3, dj.getJHK());
                   con.preparedStatement.setInt(4, dj.getLembur());
                   con.preparedStatement.setDouble(5, dj.getTotal());
                   con.preparedStatement.setDouble(6, dj.getgapok());
                   con.preparedStatement.setDouble(7, dj.getUpah_lembur());
                   con.preparedStatement.setDouble(8, dj.getTunj_jabatan());
                   con.preparedStatement.setDouble(9, dj.getTunj_kesehatan());
                   con.preparedStatement.setDouble(10, dj.getTunj_lain_lain());
                   con.preparedStatement.setDouble(11, dj.getpotongan_kehadiran());
                   con.preparedStatement.setDouble(12, dj.getpotongan_lain());
                   con.preparedStatement.setString(13, dj.getketerangan());
                   con.preparedStatement.executeUpdate();    
            }
                     
         con.dbKoneksi.commit();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            con.dbKoneksi.rollback();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    
    public String generate_nomor_otomatis(){
        try {
        String nomor="";
        Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("select distinct concat(subString(nomor,1,10), " +
"(select max(cast(substring(nomor,11,length(nomor)) as integer)) from upah_pegawai_master where subString(nomor,3,8) = (select(date_format(now(), '%Y/%m/'))))+1) " +
"from upah_pegawai_master where subString(nomor,3,8) = (select(date_format(now(), '%Y/%m/')))");
          
          if (rs.next()){
                nomor=rs.getString(1);
            }
          if(nomor.equalsIgnoreCase("")){
           rs = con.statement.executeQuery("select distinct concat('U-',(select(date_format(now(), '%Y/%m/'))),1) from master_pegawai");
           if (rs.next()){
                nomor=rs.getString(1);
            } 
          }
          con.tutupKoneksi();
          return nomor;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
        
    };
    public Vector Load(){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select u.id_upah, u.tanggal, u.nomor as noupah, periode, a.nomor as noabsen "+
                  " from upah_pegawai_master u, absensi a where a.id_absensi = u.id_absensi ");
          int i=1;
          SimpleDateFormat format = new SimpleDateFormat("MMMM YYYY");
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("id_upah"));
              row.add(rs.getDate("tanggal"));
              row.add(rs.getString("noupah"));
              row.add(format.format(rs.getDate("periode")));
              row.add(rs.getString("noabsen"));

              tableData.add(row);
              i++;
          }
          if(i==1){ 
              Vector<Object> row = new Vector<Object>();
              tableData.add(row);
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    public boolean delete(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.dbKoneksi.setAutoCommit(false);
            con.statement = con.dbKoneksi.createStatement();
            String sql  = 
                    "delete from upah_pegawai_detil where id_upah "+
                    " = "+id;
            con.statement.executeUpdate(sql);
            
            sql  = 
                    "delete from upah_pegawai_master where id_upah "+
                    " = "+id;
            con.statement.executeUpdate(sql);
            
            con.dbKoneksi.commit();
            berhasil = true;
        } catch(Exception e){
            e.printStackTrace();
            con.dbKoneksi.rollback();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
    
    public void Get_upah_pegawai_master(int id){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id_upah, tanggal, nomor, id_absensi "+
                  " from upah_pegawai_master where id_upah = '"+id+"'");
            
          int i=1;
          while(rs.next()){
              
              this.id_upah=rs.getInt("id_upah");
              this.tanggal=rs.getDate("tanggal");
              this.nomor=rs.getString("nomor");
              this.id_absensi=rs.getInt("id_absensi");
              
//tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
    public Vector Load_for_laporan(String dari, String sampai, int idPeg){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          String Filter=" and u.tanggal >= '"+dari+"' and u.tanggal <= '"+sampai+"'";
          String Filter2="";
          if (idPeg!=0){
            Filter += " and u.id_upah in (select d2.id_upah from upah_pegawai_detil d2 where id_pegawai = "+idPeg+")";
            Filter2 = " and p.idpegawai = "+idPeg;
          }
          
          
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select u.tanggal as tgl, u.nomor as noupah, periode, a.nomor as noabsen, "+
                  "   '' as namapegawai, (select sum(d.total) from upah_pegawai_detil d where d.id_upah = u.id_upah) as total, 1 urutan, u.nomor as nomorupah "+
                  " from upah_pegawai_master u, absensi a where a.id_absensi = u.id_absensi "+Filter+
                  " union all "+
                 "Select u.tanggal as tgl, p.kodePegawai as noupah, periode, a.nomor as noabsen, "+
                  "   p.namaPegawai as namapegawai, d.total as total, 2 urutan, u.nomor as nomorupah "+
                  " from upah_pegawai_master u, absensi a, upah_pegawai_detil d, master_pegawai p where p.idpegawai = d.id_pegawai and "
          + " d.id_upah = u.id_upah and a.id_absensi = u.id_absensi "+Filter+Filter2+          
          " order by tgl,nomorupah, urutan ");
          int i=1, no=0;
          SimpleDateFormat format = new SimpleDateFormat("MMMM YYYY");
          DecimalFormat formatDesimal = new DecimalFormat("#,###,###,##0.00");
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              if(rs.getString("namapegawai").equals("")) {
                no += 1;
                row.add(no);
                row.add(rs.getDate("tgl"));
                row.add(rs.getString("noupah")); 
                row.add(format.format(rs.getDate("periode")));
              } else {
                row.add("");
                row.add("");
                row.add(rs.getString("noupah"));   
                row.add(rs.getString("namapegawai"));                  
              };
                row.add(formatDesimal.format(rs.getFloat("total")));

              tableData.add(row);
              i++;
          }
          if(i==1){ 
              Vector<Object> row = new Vector<Object>();
              tableData.add(row);
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
  
}
