/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import sun.swing.table.DefaultTableCellHeaderRenderer;

/**
 *
 * @author hendry
 */
public class UpahPegawaiInput extends javax.swing.JDialog {
    int ColNo=0,
        ColIdPegawai=1,
        ColNIP=2,
        ColNama=3,
        ColJHK = 4,
        ColLembur = 5,
        Colgapok = 6,
        ColUpahLembur=7,
        ColTunjJabatan = 8,
        ColTunjKesehatan = 9,
        ColTunjLain = 10,
        ColPotKehadiran = 11,
        ColPotLain = 12,
        ColTotal = 13,
        ColKeterangan = 14;
            
    
boolean isEdit = false;
    int idUpah = 0, idAbsen=0;
    Vector<String> tableHeader = new Vector<String>();
    Vector tableData=new Vector();
    DefaultTableModel table;
    /**
     * Creates new form UpahPegawaiInput
     */
    public UpahPegawaiInput(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        tableHeader.add("No.");
        tableHeader.add("Id Pegawai");
        tableHeader.add("NIP");
        tableHeader.add("Nama Pegawai");
        tableHeader.add("JMK");
        tableHeader.add("Lembur");
        tableHeader.add("Gaji Pokok");
        tableHeader.add("Upah Lembur");
        tableHeader.add("Tunjangan Jabatan");
        tableHeader.add("Tunjangan Kesehatan");
        tableHeader.add("Tunjangan Lain - lain");
        tableHeader.add("Potongan Kehadiran");
        tableHeader.add("Potongan Lain - lain");
        tableHeader.add("Total");
        tableHeader.add("Keterangan");
        table = new DefaultTableModel(tableData,tableHeader){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return ((column>=Colgapok)&&(column<ColTotal)&&(column!=ColPotKehadiran)&&(column!=ColTunjJabatan)&&(column!=ColTunjKesehatan)); //To change body of generated methods, choose Tools | Templates.
                }
                
            };
        
        jtbInput.setModel(table);
    }
    
    public void execute(int id){
        idUpah = id;
        isEdit = (id!=0);
        if (isEdit) {
            loadData();
            btnAbsen.setVisible(false);
        }
        //btnLoadPeg.setVisible(!isEdit);
        
        this.setVisible(true);
    }
    public void loadData(){
        DBUpahpegawai tblMst = new DBUpahpegawai();
        DBUpahpegawaidetil tblDetil = new DBUpahpegawaidetil();
        DBAbsensi absen = new DBAbsensi(); 
        
        tblMst.Get_upah_pegawai_master(idUpah);
        dtpTanggal.setDate(tblMst.getTanggal());
        idAbsen = tblMst.getId_absensi();
        absen.GetDataAbsensi(idAbsen);
        txtNoAbsensi.setText(absen.getNomor());
        SimpleDateFormat format = new SimpleDateFormat("MMMM YYYY");
        txtPeriode.setText(format.format(absen.getPeriode()));
        
        tableData = tblDetil.LoadDetail(idUpah);        
        table = new DefaultTableModel(tableData,tableHeader){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return ((column>Colgapok)&&(column!=ColTotal)&&(column!=ColPotKehadiran)&&(column!=ColTunjJabatan)&&(column!=ColTunjKesehatan)); //To change body of generated methods, choose Tools | Templates.
                }
                
            };        
        jtbInput.setModel(table);

        DefaultTableCellHeaderRenderer rendererHeader = new DefaultTableCellHeaderRenderer();
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        rendererHeader.setHorizontalAlignment(JLabel.CENTER);
        renderer.setHorizontalAlignment(JLabel.RIGHT);
        jtbInput.getColumnModel().getColumn(0).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColJHK).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColNo).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColUpahLembur).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColLembur).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(Colgapok).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColTunjJabatan).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColTunjKesehatan).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColTunjLain).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColTotal).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColPotKehadiran).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColPotLain).setCellRenderer(renderer);
        jtbInput.getColumnModel().getColumn(ColIdPegawai).setMinWidth(0);
        jtbInput.getColumnModel().getColumn(ColIdPegawai).setMaxWidth(0);
        jtbInput.getColumnModel().getColumn(ColNo).setMaxWidth(30);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        btnAbsen = new javax.swing.JButton();
        txtNoAbsensi = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnSimpan = new javax.swing.JButton();
        btnBatal = new javax.swing.JButton();
        jspRekap = new javax.swing.JScrollPane();
        jtbInput = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        dtpTanggal = new org.freixas.jcalendar.JCalendarCombo();
        txtPeriode = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Upah Pegawai Input");

        jLabel3.setText("Periode");

        btnAbsen.setText("...");
        btnAbsen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbsenActionPerformed(evt);
            }
        });

        txtNoAbsensi.setEditable(false);

        jLabel4.setText("No. Absensi");

        btnSimpan.setText("Simpan");
        btnSimpan.setAlignmentX(0.5F);
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnBatal.setText("Batal");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });

        jtbInput.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Id Barang", "Nama Barang", "Qty", "Harga", "Total", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtbInput.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtbInputFocusLost(evt);
            }
        });
        jspRekap.setViewportView(jtbInput);

        jLabel2.setText("Tanggal");

        txtPeriode.setEditable(false);

        jLabel7.setText("*JMK (Jumlah Masuk Kerja)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jspRekap, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1044, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSimpan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBatal))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addGap(20, 20, 20))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNoAbsensi)
                            .addComponent(dtpTanggal, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                            .addComponent(txtPeriode))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAbsen, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(dtpTanggal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtPeriode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNoAbsensi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(btnAbsen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jspRekap, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSimpan)
                        .addComponent(btnBatal))
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAbsenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbsenActionPerformed
        // TODO add your handling code here:
        
        DBAbsensi hasil = new DBAbsensi();
        DBAbsensiDetil tblDetil = new DBAbsensiDetil();
           
        lookUpAbsensi Ainput = new lookUpAbsensi(null,true);
        hasil = Ainput.Execute();
        if (hasil.getId_absensi() != 0){
            idAbsen = hasil.getId_absensi();
            txtNoAbsensi.setText(hasil.getNomor());
              SimpleDateFormat format = new SimpleDateFormat("MMMM YYYY");

            tableData = tblDetil.LoadDetail(idAbsen,true);    
            table = new DefaultTableModel(tableData,tableHeader){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return ((column>Colgapok)&&(column!=ColTotal)&&(column!=ColPotKehadiran)&&(column!=ColTunjJabatan)&&(column!=ColTunjKesehatan)); //To change body of generated methods, choose Tools | Templates.
                }
                
            };        
            jtbInput.setModel(table);

            txtPeriode.setText(format.format(hasil.getPeriode()));

            DefaultTableCellHeaderRenderer rendererHeader = new DefaultTableCellHeaderRenderer();
            DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
            rendererHeader.setHorizontalAlignment(JLabel.CENTER);
            renderer.setHorizontalAlignment(JLabel.RIGHT);
            jtbInput.getColumnModel().getColumn(0).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColJHK).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColNo).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColUpahLembur).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColLembur).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(Colgapok).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColTunjJabatan).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColTunjKesehatan).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColTunjLain).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColTotal).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColPotKehadiran).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColPotLain).setCellRenderer(renderer);
            jtbInput.getColumnModel().getColumn(ColIdPegawai).setMinWidth(0);
            jtbInput.getColumnModel().getColumn(ColIdPegawai).setMaxWidth(0);
            jtbInput.getColumnModel().getColumn(ColNo).setMaxWidth(30);

            jtbInput.setRowSelectionInterval(0, 0);
            jtbInput.grabFocus();
        }
    }//GEN-LAST:event_btnAbsenActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        btnSimpan.grabFocus();
        DBUpahpegawai dataMst = new DBUpahpegawai();
        if (idAbsen == 0) {
            JOptionPane.showMessageDialog(null, "No. Absensi belum dipilih.");
        } else {
            java.sql.Date tgl = new java.sql.Date(dtpTanggal.getDate().getTime());
            dataMst.setTanggal(tgl);
            dataMst.setId_absensi(idAbsen);
            for(int i=0;i<jtbInput.getRowCount();i++){
                if (jtbInput.getValueAt(i, 1)!=null){
                    DBUpahpegawaidetil dataDet = new DBUpahpegawaidetil();
                    dataDet.setId_pegawai(Integer.parseInt(String.valueOf(jtbInput.getValueAt(i, ColIdPegawai))));
                    dataDet.setJHK(Integer.parseInt(String.valueOf(jtbInput.getValueAt(i, ColJHK))));
                    dataDet.setLembur(Integer.parseInt(String.valueOf(jtbInput.getValueAt(i, ColLembur))));
                    dataDet.setTotal(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTotal))));
                    dataDet.setgapok(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, Colgapok))));
                    dataDet.setUpah_lembur(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColUpahLembur))));
                    dataDet.setTunj_jabatan(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTunjJabatan))));
                    dataDet.setTunj_kesehatan(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTunjKesehatan))));
                    dataDet.setTunj_lain_lain(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTunjLain))));
                    dataDet.setpotongan_kehadiran(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColPotKehadiran))));
                    dataDet.setpotongan_lain(Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColPotLain))));
                    if(jtbInput.getValueAt(i, ColKeterangan)!=null) dataDet.setKeterangan(String.valueOf(jtbInput.getValueAt(i, ColKeterangan)).toString());
                    else  dataDet.setKeterangan("");
                    dataMst.AddUpahDetail(dataDet);
                }
            }
            if(!isEdit) {
                if (dataMst.insertMaster()) {
                    JOptionPane.showMessageDialog(null, "Penyimpanan Berhasil");
                    dispose();
                }
                else JOptionPane.showMessageDialog(null, "Penyimpanan Gagal");
            } else {
                dataMst.setId_upah(idUpah);
                if (dataMst.editMaster()) {
                    JOptionPane.showMessageDialog(null, "Update Berhasil");
                    dispose();
                }
                else JOptionPane.showMessageDialog(null, "Update Gagal");
            }
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnBatalActionPerformed

    private void jtbInputFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtbInputFocusLost
        // TODO add your handling code here:
        Double total=0.00;
        for(int i=0;i<jtbInput.getRowCount();i++){
            total = 0.00;
                    if (jtbInput.getValueAt(i, 5)!=null){
                        try{
                        total+=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, Colgapok)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        try{
                        total+=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColLembur)))*Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColUpahLembur)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        try{
                        total+=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTunjJabatan)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        try{
                        total+=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTunjKesehatan)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        try{
                        total+=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColTunjLain)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        try{
                        total-=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColPotKehadiran)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        try{
                        total-=Double.parseDouble(String.valueOf(jtbInput.getValueAt(i, ColPotLain)));
                        }catch(Exception e){
                            
                        }finally{
                            
                        }
                        
                        jtbInput.setValueAt(total, i, ColTotal);                        
                    }
                }
    }//GEN-LAST:event_jtbInputFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpahPegawaiInput.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpahPegawaiInput.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpahPegawaiInput.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpahPegawaiInput.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UpahPegawaiInput dialog = new UpahPegawaiInput(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbsen;
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnSimpan;
    private org.freixas.jcalendar.JCalendarCombo dtpTanggal;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jspRekap;
    private javax.swing.JTable jtbInput;
    private javax.swing.JTextField txtNoAbsensi;
    private javax.swing.JTextField txtPeriode;
    // End of variables declaration//GEN-END:variables
}
