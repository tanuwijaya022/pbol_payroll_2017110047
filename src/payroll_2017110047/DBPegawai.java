/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll_2017110047;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author Multimedia
 */
public class DBPegawai {
    int idPegawai,idJabatan;
    String kodePegawai, namaPegawai, alamat, noHp;
    Date tglLahir;
    private Double Gapok, TunjKesehatan;

    public Double getGapok() {
        return Gapok;
    }

    public void setGapok(Double Gapok) {
        this.Gapok = Gapok;
    }

    public Double getTunjKesehatan() {
        return TunjKesehatan;
    }

    public void setTunjKesehatan(Double TunjKesehatan) {
        this.TunjKesehatan = TunjKesehatan;
    }

    
    public int getIdPegawai() {
        return idPegawai;
    }

    public void setIdPegawai(int idPegawai) {
        this.idPegawai = idPegawai;
    }

    public String getKodePegawai() {
        return kodePegawai;
    }

    public void setKodePegawai(String kodePegawai) {
        this.kodePegawai = kodePegawai;
    }

    public String getNamaPegawai() {
        return namaPegawai;
    }

    public void setNamaPegawai(String namaPegawai) {
        this.namaPegawai = namaPegawai;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public int getIdJabatan() {
        return idJabatan;
    }

    public void setIdJabatan(int idJabatan) {
        this.idJabatan = idJabatan;
    }

    public Date getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Date tglLahir) {
        this.tglLahir = tglLahir;
    }
    
    
    
    public void GetAllField(String fld, String dt){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idPegawai, kodePegawai, namaPegawai, alamat, "+
                    " noHp, idJabatan, tglLahir, TunjKesehatan, Gapok "+
                    " from master_pegawai where "+fld+" = '"+dt+"'");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              
              this.idPegawai=rs.getInt("idPegawai");
              this.kodePegawai=rs.getString("kodePegawai");
              this.namaPegawai=rs.getString("namaPegawai");
              this.alamat=rs.getString("alamat");
              this.noHp=rs.getString("noHp");
              this.idJabatan=rs.getInt("idJabatan");
              this.tglLahir=rs.getDate("tglLahir");
              this.TunjKesehatan=rs.getDouble("TunjKesehatan");
              this.Gapok=rs.getDouble("Gapok");
              
//tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    
    public Vector LookUp(String dt, String idPegExist, String tgl){
        try{
            Vector tableData= new Vector();
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idPegawai, kodePegawai, namaPegawai, alamat, "+
                    " noHp, idJabatan, tglLahir, TunjKesehatan "+
                    " from master_pegawai where ((kodePegawai = '"+dt+"') or (namaPegawai = '"+dt+"')) and idPegawai not in ("+idPegExist+") ");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              if (Pegawai_sudah_ada_pada_periode(rs.getInt("idPegawai"),tgl)) JOptionPane.showMessageDialog(null, "Pegawai sudah pernah diinput pada periode terpilih.");
              tableData.add(i);
              tableData.add(rs.getInt("idPegawai"));
              tableData.add(rs.getString("kodePegawai"));
              tableData.add(rs.getString("namaPegawai"));
              i++;
          }
          con.tutupKoneksi();
          if (i>1) return tableData;
          else return null;
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    } 
    
    public boolean Pegawai_sudah_ada_pada_periode(int idPeg, String tgl){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select count(*) as jml "+
                    " from absensi m, detil_absensi d where m.id_absensi = d.id_absensi and id_pegawai = "+idPeg+" and periode = LAST_DAY('"+tgl+"')");
          int jml=0;  
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              jml = (rs.getInt("jml"));
          }
          con.tutupKoneksi();
          return jml>0;
          } catch (SQLException ex){
            ex.printStackTrace();
          return true;
        }
    }
    /*public static String LookUp(String fld, String dt){
        try{
            String hasil="";
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idBarang, NamaBarang, harga "+
                    " from barang where "+fld+" = '%"+dt+"%'");
            
          int i=1;
          while(rs.next()){
              hasil=(rs.getString("idBarang"))+(rs.getString("NamaBarang"))+(rs.getFloat("harga"));
              i++;
          }
          con.tutupKoneksi();
          return hasil;
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }*/
    
    public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into master_pegawai"+
                    "(idPegawai, kodePegawai, namaPegawai, alamat, "+
                    " noHp, idJabatan, tglLahir, TunjKesehatan, Gapok) values (?,?,?,?,?,?,?,?,?)");
                     con.preparedStatement.setInt(1, this.idPegawai);
                     con.preparedStatement.setString(2, this.kodePegawai);
                     con.preparedStatement.setString(3, this.namaPegawai);
                     con.preparedStatement.setString(4, this.alamat);
                     con.preparedStatement.setString(5, this.noHp);
                     con.preparedStatement.setInt(6, this.idJabatan);
                     con.preparedStatement.setDate(7, this.tglLahir);
                     con.preparedStatement.setDouble(8, this.TunjKesehatan);
                     con.preparedStatement.setDouble(9, this.Gapok);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    public Vector Load(){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select idPegawai,kodePegawai, namaPegawai, alamat, "+
                    " noHp, j.namaJabatan, tglLahir "+
                  " from master_pegawai p, master_jabatan j where j.idJabatan = p.idJabatan");
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("idPegawai"));
              row.add(rs.getString("kodePegawai"));
              row.add(rs.getString("namaPegawai"));
              row.add(rs.getString("alamat"));
              row.add(rs.getString("noHp"));
              row.add(rs.getString("j.namaJabatan"));
              row.add(rs.getDate("tglLahir"));
              tableData.add(row);
              i++;
          }
          if(i==1){ 
              Vector<Object> row = new Vector<Object>();
              tableData.add(row);
          }
          
          
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    public Vector Load4Pegawai(int JHK, String tgl){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("Select idPegawai,kodePegawai, namaPegawai "+
                  " from master_pegawai p where p.idPegawai not in (select id_pegawai from absensi m, detil_absensi d where "+
                  " m.id_absensi = d.id_absensi and d.id_pegawai = p.idPegawai and LAST_DAY(m.periode) = LAST_DAY('"+tgl+"'))");
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("idPegawai"));
              row.add(rs.getString("kodePegawai"));
              row.add(rs.getString("namaPegawai"));
              row.add(JHK);
              row.add(JHK);
              tableData.add(row);
              i++;
          }
          if(i==1){ 
              Vector<Object> row = new Vector<Object>();
              tableData.add(row);
          }
          
          
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    public boolean delete(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("delete from master_pegawai where idPegawai"+
                    " = ? ");
            con.preparedStatement.setInt(1, id);
            con.preparedStatement.executeUpdate();
            berhasil = true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
    public boolean select(int id){
        try {
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idPegawai, kodePegawai, namaPegawai, alamat, "+
                    " noHp, idJabatan, tglLahir, TunjKesehatan, Gapok from master_pegawai where idPegawai = "+id);
            while (rs.next()){
              this.idPegawai=rs.getInt("idPegawai");
              this.kodePegawai=rs.getString("kodePegawai");
              this.namaPegawai=rs.getString("namaPegawai");
              this.alamat=rs.getString("alamat");
              this.noHp=rs.getString("noHp");
              this.idJabatan=rs.getInt("idJabatan");
              this.tglLahir=rs.getDate("tglLahir");
              this.TunjKesehatan=rs.getDouble("TunjKesehatan");
              this.Gapok=rs.getDouble("Gapok");
            }
            con.tutupKoneksi();
            return true;
        } catch(SQLException ex){
            ex.printStackTrace();
            return false;
        }
    }
    
    public int validasiPegawai(String nama, int idKecuali){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("Select count(*) as jml "+
         " from master_pegawai where kodePegawai = '"+nama+"' and idPegawai <> "+idKecuali);
            while(rs.next()){
                val = rs.getInt("jml");
            }
            con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();                    
        }

        return val;
    }
    
    public boolean update(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("update master_pegawai set kodePegawai=?, namaPegawai=?, alamat=?, "+
                    " noHp=?, idJabatan=?, tglLahir=?, TunjKesehatan=?, Gapok=? "+
                    " where idPegawai = ?");
                     con.preparedStatement.setString(1, this.kodePegawai);
                     con.preparedStatement.setString(2, this.namaPegawai);
                     con.preparedStatement.setString(3, this.alamat);
                     con.preparedStatement.setString(4, this.noHp);
                     con.preparedStatement.setInt(5, this.idJabatan);
                     con.preparedStatement.setDate(6, this.tglLahir);
                     con.preparedStatement.setDouble(7, this.TunjKesehatan);
                     con.preparedStatement.setDouble(8, this.Gapok);
                     con.preparedStatement.setInt(9, this.idPegawai);
                     con.preparedStatement.executeUpdate();
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    public Vector LookUp(String dt){
        try{
            Vector tableData= new Vector();
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idPegawai, kodePegawai, namaPegawai "+
                    " from master_pegawai where ((upper(kodePegawai) like upper('%"+dt+"%')) or (upper(namaPegawai) like upper('%"+dt+"%')))");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(i);
              row.add(rs.getInt("idPegawai"));
              row.add(rs.getString("kodePegawai"));
              row.add(rs.getString("namaPegawai"));
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          if (i>1) return tableData;
          else return null;
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    } 
    public void GetDataPegawai(int dt){
        try{
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select idPegawai, kodePegawai, namaPegawai "+
                    " from master_pegawai where idPegawai = "+dt);
            
          int i=1;
          while(rs.next()){
              this.idPegawai = rs.getInt("idPegawai");
              this.kodePegawai = rs.getString("kodePegawai");
              this.namaPegawai = rs.getString("namaPegawai");
              i++;
          }
          con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
}
